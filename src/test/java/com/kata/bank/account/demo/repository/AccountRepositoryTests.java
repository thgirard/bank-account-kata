package com.kata.bank.account.demo.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.kata.bank.account.demo.domain.AccountEntity;
import com.kata.bank.account.demo.domain.UserEntity;

@RunWith(SpringRunner.class)
@DataJpaTest
public class AccountRepositoryTests {

  	@Autowired
    private TestEntityManager entityManager;
  	
  	@Autowired
  	private AccountRepository accountRepository;
  	
  	
  	@Test
  	public void whenFindByAccountNumber_thenAccount() {

  		UserEntity user = new UserEntity();
		user.setFirstName("thibault");
		user.setLastName("girard");
		user.setEmail("test@test.fr");
		user.setUsername("tgirard");
		user.setPassword("fdsfqsdfqsdfqs");
		entityManager.persist(user);
		entityManager.flush();
		
		AccountEntity account = new AccountEntity();
		account.setAccountBalance(new BigDecimal("20.0"));
		account.setAccountNumber(25); 		
  	    entityManager.persist(account);
  	    entityManager.flush();
 
  	    Optional<AccountEntity> found = accountRepository.findByAccountNumber(25);
  	 
  	    assertThat(found.get().getAccountBalance())
  	      .isEqualTo(new BigDecimal("20.0"));
  	}
  	
  	@Test
  	public void whenFindByAccountNumber_thenNoAccount() {

  		UserEntity user = new UserEntity();
		user.setFirstName("thibault");
		user.setLastName("girard");
		user.setEmail("test@test.fr");
		user.setUsername("tgirard");
		user.setPassword("fdsfqsdfqsdfqs");
		entityManager.persist(user);
		entityManager.flush();
		
		AccountEntity account = new AccountEntity();
		account.setAccountBalance(new BigDecimal("20.0"));
		account.setAccountNumber(25); 		
  	    entityManager.persist(account);
  	    entityManager.flush();
 
  	    Optional<AccountEntity> found = accountRepository.findByAccountNumber(35);
  	 
  	    assertThat(found.isEmpty());
  	}
}

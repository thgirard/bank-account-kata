package com.kata.bank.account.demo.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.time.Instant;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.kata.bank.account.demo.domain.AccountEntity;
import com.kata.bank.account.demo.domain.TransactionEntity;
import com.kata.bank.account.demo.domain.UserEntity;
import com.kata.bank.account.demo.repository.TransactionRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TransactionServiceTests {

	@TestConfiguration
	static class TransactionServiceTestContextConfiguration {

		@Bean
		public TransactionService transactionService() {
			return new TransactionService();
		}

	}


	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private TransactionService transactionService;
	
	
	@Autowired
	private TransactionRepository transactionRepository;
	
	
	@Test()
	public void whenSaveDeposit_thenTransactionIsSaved() {

		UserEntity user = new UserEntity();
		user.setFirstName("thibault");
		user.setLastName("girard");
		user.setEmail("test@test.fr");
		user.setUsername("tgirard");
		user.setPassword("fdsfqsdfqsdfqs");
		entityManager.persist(user);
		entityManager.flush();
		
		AccountEntity account = new AccountEntity();
		account.setAccountBalance(new BigDecimal(20.0));
		account.setAccountNumber(25); 		
  	    entityManager.persist(account);
  	    entityManager.flush();
 
  	    TransactionEntity t1 = new TransactionEntity(Instant.now(), "Deposit", "Deposit", "10", new BigDecimal(10.0), account);
  	    
		transactionService.saveDepositTransaction(t1);
		
		assertThat(!transactionRepository.findAll().isEmpty());
		
	}
	
	@Test()
	public void whenSaveWithDraw_thenTransactionIsSaved() {

		UserEntity user = new UserEntity();
		user.setFirstName("thibault");
		user.setLastName("girard");
		user.setEmail("test@test.fr");
		user.setUsername("tgirard");
		user.setPassword("fdsfqsdfqsdfqs");
		entityManager.persist(user);
		entityManager.flush();
		
		AccountEntity account = new AccountEntity();
		account.setAccountBalance(new BigDecimal(20.0));
		account.setAccountNumber(25); 		
  	    entityManager.persist(account);
  	    entityManager.flush();
 
  	    TransactionEntity t1 = new TransactionEntity(Instant.now(), "Withdraw", "Withdraw", "10", new BigDecimal(10.0), account);
  	    
		transactionService.saveWithdrawTransaction(t1);
		
		assertThat(!transactionRepository.findAll().isEmpty());
		
	}
}

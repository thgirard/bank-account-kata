package com.kata.bank.account.demo.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.security.Principal;
import java.security.SecureRandom;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import com.kata.bank.account.demo.domain.UserEntity;
import com.kata.bank.account.demo.repository.AccountRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class AccountServiceTests {

	@TestConfiguration
	static class AccountServiceTestContextConfiguration {

		@Bean
		public UserService userService() {
			return new UserService();
		}

		@Bean
		public AccountService accountService() {
			return new AccountService();
		}

		@Bean
		public TransactionService transactionService() {
			return new TransactionService();
		}

		@Bean
		public BCryptPasswordEncoder passwordEncoder() {
			return new BCryptPasswordEncoder(9, new SecureRandom("test".getBytes()));
		}
	}

	@Autowired
	private AccountService accountService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private AccountRepository accountRepository;
	
	
	@Test()
	public void whenCreateAccount_thenAccountIsSaved() {

		int accountNumber = accountService.createAccount().getAccountNumber();
		
		assertThat(accountRepository.findByAccountNumber(accountNumber).isPresent());
		
	}
	
	@Test()
	public void whenDeposit_thenAccountHavePositiveBalance() {

		UserEntity user = new UserEntity();
		user.setFirstName("thibault");
		user.setLastName("girard");
		user.setEmail("test@test.fr");
		user.setUsername("tgirard");
		user.setPassword("fdsfqsdfqsdfqs");

		userService.createUser(user);

		accountService.deposit("10", new Principal() {
			
			@Override
			public String getName() {
				return "tgirard";
			}
		});;
		
		assertThat(userService.findByUsername("tgirard").getAccount().getAccountBalance()).isEqualTo(new BigDecimal("10.0"));
		
	}
	
	
	
	@Test()
	public void whenWithDraw_thenAccountHaveGoodBalance() {


		UserEntity user = new UserEntity();
		user.setFirstName("thibault");
		user.setLastName("girard");
		user.setEmail("test@test.fr");
		user.setUsername("tgirard");
		user.setPassword("fdsfqsdfqsdfqs");

		userService.createUser(user);

		accountService.deposit("10", new Principal() {
			
			@Override
			public String getName() {
				return "tgirard";
			}
		});;
		
		accountService.withdraw("8", new Principal() {
			
			@Override
			public String getName() {
				return "tgirard";
			}
		});;
		
		assertThat(userService.findByUsername("tgirard").getAccount().getAccountBalance()).isEqualTo(new BigDecimal("2.0"));
		
	}
	
}	
package com.kata.bank.account.demo.configuration;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SecurityWebMvcConfigTests {

	@Autowired
	private WebApplicationContext context;

	private MockMvc mvc;

	@Before
	public void setup() {
		mvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
	}

	@WithMockUser(value = "spring")
	@Test
	public void givenAuthRequestOnPrivateService_shouldSucceedWith200() throws Exception {
		mvc.perform(get("/index")).andExpect(status().isOk());
	}
	
	@WithMockUser(value = "spring")
	@Test
	public void givenAuthRequestOnPrivateService_shouldFailedWith401() throws Exception {
		mvc.perform(get("/account")).andExpect(status().is4xxClientError());
	}

	@WithMockUser(value = "spring")
	@Test
	public void givenAuthRequestOnPrivateServices_shouldSucceedWith200or500() throws Exception {
		mvc.perform(get("/bootstrap/css/bootstrap.min.css")).andExpect(status().isOk());
		mvc.perform(get("/jquery/jquery.js")).andExpect(status().isOk());
		mvc.perform(get("/error")).andExpect(status().isInternalServerError());
		mvc.perform(get("/signup")).andExpect(status().isOk());
	}
}

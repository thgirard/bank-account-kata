package com.kata.bank.account.demo.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.security.Principal;
import java.security.SecureRandom;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.kata.bank.account.demo.domain.UserEntity;
import com.kata.bank.account.demo.service.AccountService;
import com.kata.bank.account.demo.service.TransactionService;
import com.kata.bank.account.demo.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountControllerTests {

	@Autowired
	private UserService userservice;

	@Autowired
	private WebApplicationContext context;

	private MockMvc mvc;

	@Before
	public void setup() {
		mvc = MockMvcBuilders.webAppContextSetup(context).build();
	}

	@Test
	public void accountMvcHomeControler_shouldSend200() throws Exception {
		UserEntity user = new UserEntity();
		user.setFirstName("thibault");
		user.setLastName("girard");
		user.setEmail("test@test.fr");
		user.setUsername("tgirard");
		user.setPassword("fdsfqsdfqsdfqs");

		userservice.createUser(user);

		Principal principal = new Principal() {
	        @Override
	        public String getName() {
	            return "tgirard";
	        }
	    };
	    
		mvc.perform(get("/account/account").principal(principal)).andExpect(status().isOk()).andExpect(view().name("account"))
				.andExpect(model().attribute("account", notNullValue()))
				.andExpect(model().attribute("transactionList", notNullValue()));
	}

	@Test
	public void depositMvcHomeControler_shouldSend200() throws Exception {

		mvc.perform(get("/account/deposit")).andExpect(status().isOk()).andExpect(view().name("deposit"))
				.andExpect(model().attribute("amount", notNullValue()));
	}

	@Test
	public void withdrawMvcHomeControler_shouldSend200() throws Exception {

		mvc.perform(get("/account/withdraw")).andExpect(status().isOk()).andExpect(view().name("withdraw"))
				.andExpect(model().attribute("amount", notNullValue()));
	}
	
	@Test
	public void postDepositMvcHomeControler_shouldSend302() throws Exception {
		UserEntity user = new UserEntity();
		user.setFirstName("thibault1");
		user.setLastName("girard1");
		user.setEmail("test1@test.fr");
		user.setUsername("tgirard1");
		user.setPassword("fdsfqsdfqsdfqs");
		
		userservice.createUser(user);

		Principal principal = new Principal() {
	        @Override
	        public String getName() {
	            return "tgirard1";
	        }
	    };
		
		mvc.perform(post("/account/deposit")
				.flashAttr("amount", "10").principal(principal)).andExpect(status().is3xxRedirection())
				.andExpect(view().name("redirect:/home"));
	}
	
	public void postDepositMvcHomeControler_shouldSendError200() throws Exception {
		
		mvc.perform(post("/account/deposit")
				.flashAttr("amount", "afdqfqsd")).andExpect(status().isOk())
				.andExpect(view().name("deposit"))
				.andExpect(model().attribute("errordeposit", notNullValue()));

	}
	
	@Test
	public void postWithdrawMvcHomeControler_shouldSend302() throws Exception {
		UserEntity user = new UserEntity();
		user.setFirstName("thibault2");
		user.setLastName("girard2");
		user.setEmail("test2@test.fr");
		user.setUsername("tgirard2");
		user.setPassword("fdsfqsdfqsdfqs");
		
		userservice.createUser(user);

		Principal principal = new Principal() {
	        @Override
	        public String getName() {
	            return "tgirard2";
	        }
	    };
	    
		mvc.perform(post("/account/deposit")
				.flashAttr("amount", "10").principal(principal));
		
		mvc.perform(post("/account/withdraw")
				.flashAttr("amount", "1").principal(principal)).andExpect(status().is3xxRedirection())
				.andExpect(view().name("redirect:/home"));
	}
	
	public void postWithdrawMvcHomeControler_shouldSendError200() throws Exception {
		UserEntity user = new UserEntity();
		user.setFirstName("thibault3");
		user.setLastName("girard3");
		user.setEmail("test3@test.fr");
		user.setUsername("tgirard3");
		user.setPassword("fdsfqsdfqsdfqs");
		
		userservice.createUser(user);

		Principal principal = new Principal() {
	        @Override
	        public String getName() {
	            return "tgirard3";
	        }
	    };
	    
		mvc.perform(post("/account/deposit")
				.flashAttr("amount", "10").principal(principal));
		
		mvc.perform(post("/account/withdraw")
				.flashAttr("amount", "afdqfqsd").principal(principal)).andExpect(status().isOk())
				.andExpect(view().name("withdraw"))
				.andExpect(model().attribute("errorwithdraw", notNullValue()));

	}
	
	public void postWithdrawMvcHomeControler_shouldSendErrorBalance200() throws Exception {
		
		UserEntity user = new UserEntity();
		user.setFirstName("thibault4");
		user.setLastName("girard4");
		user.setEmail("test4@test.fr");
		user.setUsername("tgirard4");
		user.setPassword("fdsfqsdfqsdfqs");
		
		userservice.createUser(user);

		Principal principal = new Principal() {
	        @Override
	        public String getName() {
	            return "tgirard4";
	        }
	    };
	    
		mvc.perform(post("/account/deposit")
				.flashAttr("amount", "1").principal(principal));
		
		mvc.perform(post("/account/withdraw")
				.flashAttr("amount", "10").principal(principal)).andExpect(status().isOk())
				.andExpect(view().name("withdraw"))
				.andExpect(model().attribute("errorbalance", notNullValue()));

	}
	
}

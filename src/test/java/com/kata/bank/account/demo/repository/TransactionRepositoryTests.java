package com.kata.bank.account.demo.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.kata.bank.account.demo.domain.AccountEntity;
import com.kata.bank.account.demo.domain.TransactionEntity;
import com.kata.bank.account.demo.domain.UserEntity;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TransactionRepositoryTests {

  	@Autowired
    private TestEntityManager entityManager;
  	
  	@Autowired
  	private TransactionRepository transactionRepository;
  	
  	
  	@Test
  	public void whenFindAll_thenReturnNoTransaction() {

  	    List<TransactionEntity> found = transactionRepository.findAll();

  	    assertThat(found.isEmpty());
  	}
  	
 	public void whenFindAll_thenReturnAllTransactions() {

  		UserEntity user = new UserEntity();
		user.setFirstName("thibault");
		user.setLastName("girard");
		user.setEmail("test@test.fr");
		user.setUsername("tgirard");
		user.setPassword("fdsfqsdfqsdfqs");
		entityManager.persist(user);
		entityManager.flush();
		
		AccountEntity account = new AccountEntity();
		account.setAccountBalance(new BigDecimal(20.0));
		account.setAccountNumber(25); 		
  	    entityManager.persist(account);
  	    entityManager.flush();
 
  	    TransactionEntity t1 = new TransactionEntity(Instant.now(), "Deposit", "Deposit", "10", new BigDecimal(10.0), account);
  	    entityManager.persist(t1);
  	    entityManager.flush();
  	    
  	    TransactionEntity t2 = new TransactionEntity(Instant.now(), "Deposit", "Deposit", "20", new BigDecimal(10.0), account);
  	    entityManager.persist(t2);
  	    entityManager.flush();
  	 
  	    List<TransactionEntity> found = transactionRepository.findAll();
  	 
  	    assertThat(found.size() == 2);
  	}
}

package com.kata.bank.account.demo.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.security.SecureRandom;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import com.kata.bank.account.demo.domain.UserEntity;
import com.kata.bank.account.demo.exception.UserException;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserServiceTests {

	@TestConfiguration
	static class UserServiceTestContextConfiguration {

		@Bean
		public UserService userService() {
			return new UserService();
		}

		@Bean
		public AccountService accountService() {
			return new AccountService();
		}

		@Bean
		public TransactionService transactionService() {
			return new TransactionService();
		}

		@Bean
		public BCryptPasswordEncoder passwordEncoder() {
			return new BCryptPasswordEncoder(9, new SecureRandom("test".getBytes()));
		}
	}

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private UserService userservice;

	@Test
	public void whenFindByUsername_thenReturnUser() {

		UserEntity user = new UserEntity();
		user.setFirstName("thibault");
		user.setLastName("girard");
		user.setEmail("test@test.fr");
		user.setUsername("tgirard");
		user.setPassword("fdsfqsdfqsdfqs");
		entityManager.persist(user);
		entityManager.flush();

		assertThat(userservice.findByUsername("tgirard").getFirstName()).isEqualTo("thibault");

	}

	@Test(expected = UserException.class)
	public void whenFindByUsername_thenThrowException() {

		userservice.findByUsername("tgirard");

	}

	@Test
	public void whenCreateUser_thenUserIsSaved() {

		UserEntity user = new UserEntity();
		user.setFirstName("thibault");
		user.setLastName("girard");
		user.setEmail("test@test.fr");
		user.setUsername("tgirard");
		user.setPassword("fdsfqsdfqsdfqs");

		userservice.createUser(user);

		assertThat(userservice.findByUsername("tgirard").getFirstName()).isEqualTo("thibault");

	}

	@Test
	public void whenCheckUsernameExist_thenUserExist() {

		UserEntity user = new UserEntity();
		user.setFirstName("thibault");
		user.setLastName("girard");
		user.setEmail("test@test.fr");
		user.setUsername("tgirard");
		user.setPassword("fdsfqsdfqsdfqs");

		entityManager.persist(user);
		entityManager.flush();

		assertThat(userservice.checkUsernameExists("tgirard"));

	}

	@Test
	public void whenCheckUsernameExist_thenNoUser() {

		assertThat(!userservice.checkUsernameExists("tgirard"));

	}

	@Test
	public void whenCheckEmailExist_thenUserExist() {

		UserEntity user = new UserEntity();
		user.setFirstName("thibault");
		user.setLastName("girard");
		user.setEmail("test@test.fr");
		user.setUsername("tgirard");
		user.setPassword("fdsfqsdfqsdfqs");

		entityManager.persist(user);
		entityManager.flush();

		assertThat(userservice.checkEmailExists("test@test.fr"));

	}

	@Test
	public void whenCheckEmailExist_thenNoUser() {

		assertThat(!userservice.checkEmailExists("test@test.fr"));

	}
}

package com.kata.bank.account.demo.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.kata.bank.account.demo.domain.UserEntity;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTests {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private UserRepository userRepository;

	@Test
	public void whenFindByEmail_thenReturnUser() {

		UserEntity user = new UserEntity();
		user.setFirstName("thibault");
		user.setLastName("girard");
		user.setEmail("test@test.fr");
		user.setUsername("tgirard");
		user.setPassword("fdsfqsdfqsdfqs");
		entityManager.persist(user);
		entityManager.flush();

		Optional<UserEntity> found = userRepository.findByEmail("test@test.fr");

		assertThat(found.get().getFirstName()).isEqualTo("thibault");
	}

	@Test
	public void whenFindByEmail_thenReturnNoUser() {

		UserEntity user = new UserEntity();
		user.setFirstName("thibault");
		user.setLastName("girard");
		user.setEmail("test@test.fr");
		user.setUsername("tgirard");
		user.setPassword("fdsfqsdfqsdfqs");
		entityManager.persist(user);
		entityManager.flush();

		Optional<UserEntity> found = userRepository.findByEmail("testfalse@test.fr");

		assertThat(found.isEmpty());
	}

	@Test
	public void whenFindByUserName_thenReturnUser() {

		UserEntity user = new UserEntity();
		user.setFirstName("thibault");
		user.setLastName("girard");
		user.setEmail("test@test.fr");
		user.setUsername("tgirard");
		user.setPassword("fdsfqsdfqsdfqs");
		entityManager.persist(user);
		entityManager.flush();

		Optional<UserEntity> found = userRepository.findByUsername("tgirard");

		assertThat(found.get().getFirstName()).isEqualTo("thibault");
	}

	@Test
	public void whenFindByUserName_thenReturnNoUser() {

		UserEntity user = new UserEntity();
		user.setFirstName("thibault");
		user.setLastName("girard");
		user.setEmail("test@test.fr");
		user.setUsername("tgirard");
		user.setPassword("fdsfqsdfqsdfqs");
		entityManager.persist(user);
		entityManager.flush();

		Optional<UserEntity> found = userRepository.findByUsername("testfalse");

		assertThat(found.isEmpty());
	}

	@Test
	public void whenFindAll_thenReturnNoUser() {

		List<UserEntity> found = userRepository.findAll();

		assertThat(found.isEmpty());
	}
	
	@Test
	public void whenFindByUserName_thenReturnAllUser() {

		UserEntity user = new UserEntity();
		user.setFirstName("thibault");
		user.setLastName("girard");
		user.setEmail("test@test.fr");
		user.setUsername("tgirard");
		user.setPassword("fdsfqsdfqsdfqs");
		entityManager.persist(user);
		entityManager.flush();
		
		UserEntity user1 = new UserEntity();
		user1.setFirstName("thibault1");
		user1.setLastName("girard1");
		user1.setEmail("test1@test.fr");
		user1.setUsername("tgirard1");
		user1.setPassword("fdsfqsdfqsdfqs");
		entityManager.persist(user1);
		entityManager.flush();

		List<UserEntity> found = userRepository.findAll();

		assertThat(found.size()==2);
	}

}

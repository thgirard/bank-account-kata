package com.kata.bank.account.demo.controller;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.security.Principal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.kata.bank.account.demo.domain.UserEntity;
import com.kata.bank.account.demo.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest	
public class HomeControllerTests {

	@Autowired
	private UserService userservice;

	@Autowired
	private WebApplicationContext context;

	private MockMvc mvc;
	
	@Before
	public void setup() {
		mvc = MockMvcBuilders.webAppContextSetup(context).build();
	}

	@Test
	public void defaultMvcHomeControler_shouldRedirect() throws Exception {
		mvc.perform(get("/")).andExpect(status().is3xxRedirection()).andExpect(view().name("redirect:/index"));
	}

	@Test
	public void indexMvcHomeControler_shouldSend200() throws Exception {
		mvc.perform(get("/index")).andExpect(status().isOk()).andExpect(view().name("index"));
	}

	@Test
	public void signupMvcHomeControler_shouldSend200() throws Exception {
		mvc.perform(get("/signup")).andExpect(status().isOk()).andExpect(view().name("signup"))
				.andExpect(model().attribute("user", notNullValue()));
	}

	@Test
	public void postSignupMvcHomeControler_shouldSend302() throws Exception {
		UserEntity user = new UserEntity();
		user.setFirstName("thibault1");
		user.setLastName("girard1");
		user.setEmail("test11@test.fr");
		user.setUsername("tgirard11");
		user.setPassword("fdsfqsdfqsdfqs");
		
		mvc.perform(post("/signup")
				.flashAttr("user", user)).andExpect(status().is3xxRedirection())
				.andExpect(view().name("redirect:/"));
	}


	@Test
	public void postSignupEmailExistMvcHomeControler_shouldSend200() throws Exception {
		UserEntity user = new UserEntity();
		user.setFirstName("thibault12");
		user.setLastName("girard12");
		user.setEmail("test12@test.fr");
		user.setUsername("tgirard12");
		user.setPassword("fdsfqsdfqsdfqs");
		
		userservice.createUser(user);
		
		user.setUsername("tgirard15");
		
		mvc.perform(post("/signup")
				.flashAttr("user", user)).andExpect(status().isOk())
				.andExpect(view().name("signup")).andExpect(model().attribute("emailExists", notNullValue()))
				.andExpect(model().attribute("usernameExists", nullValue()));
	}
	
	@Test
	public void postSignupUserExistMvcHomeControler_shouldSend200() throws Exception {
		UserEntity user = new UserEntity();
		user.setFirstName("thibault4");
		user.setLastName("girard4");
		user.setEmail("test14@test.fr");
		user.setUsername("tgirard14");
		user.setPassword("fdsfqsdfqsdfqs");
		
		userservice.createUser(user);
		
		user.setEmail("test15@test.fr");
		
		mvc.perform(post("/signup")
				.flashAttr("user", user)).andExpect(status().isOk())
				.andExpect(view().name("signup")).andExpect(model().attribute("emailExists", nullValue()))
				.andExpect(model().attribute("usernameExists", notNullValue()));
	}
	
	@Test
	public void homeMvcHomeControler_shouldSend200() throws Exception {

		UserEntity user = new UserEntity();
		user.setFirstName("thibault3");
		user.setLastName("girard3");
		user.setEmail("test13@test.fr");
		user.setUsername("tgirard13");
		user.setPassword("fdsfqsdfqsdfqs");

		userservice.createUser(user);
		
		Principal principal = new Principal() {
	        @Override
	        public String getName() {
	            return "tgirard13";
	        }
	    };

		mvc.perform(get("/home").principal(principal)).andExpect(status().isOk()).andExpect(view().name("home"))
				.andExpect(model().attribute("account", notNullValue()));
	}

}

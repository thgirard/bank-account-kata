package com.kata.bank.account.demo.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.kata.bank.account.demo.domain.TransactionEntity;


/**
 * Transaction repository.
 */
public interface TransactionRepository extends CrudRepository<TransactionEntity, Long> {

    /**
     * Find all transactions     
     *
     * @return The list of all transactions
     * */
	List<TransactionEntity> findAll();
	
}

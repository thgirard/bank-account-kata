package com.kata.bank.account.demo.controller;

import java.math.BigDecimal;
import java.security.Principal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.kata.bank.account.demo.domain.AccountEntity;
import com.kata.bank.account.demo.domain.TransactionEntity;
import com.kata.bank.account.demo.domain.UserEntity;
import com.kata.bank.account.demo.service.AccountService;
import com.kata.bank.account.demo.service.UserService;

/**
 * Account controller.
 */
@Controller
@RequestMapping("/account")
public class AccountController {

	/**
	 * Logger.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(AccountController.class);

	/**
	 * User service
	 */
	@Autowired
	private UserService userService;

	/**
	 * Account service
	 */
	@Autowired
	private AccountService accountService;

	/**
	 * Spring MVC implementation of getting the actual account balance and
	 * transactions history.
	 *
	 * @param model.
	 * @param principal.
	 * @return A Spring response to specify the thymeleaf template to used.
	 */
	@RequestMapping("/account")
	public String Account(Model model, Principal principal) {

		UserEntity user = userService.findByUsername(principal.getName());
		AccountEntity account = user.getAccount();
		List<TransactionEntity> transactionList = account.getTransactionList();

		model.addAttribute("account", account);
		model.addAttribute("transactionList", transactionList);

		return "account";
	}

	/**
	 * Spring MVC implementation of getting the page to make a deposit.
	 *
	 * @param model.
	 * @return A Spring response to specify the thymeleaf template to used.
	 */
	@RequestMapping(value = "/deposit", method = RequestMethod.GET)
	public String deposit(Model model) {
		model.addAttribute("amount", "");

		return "deposit";
	}

	/**
	 * Spring MVC implementation of making a deposit.
	 *
	 * @param amount.
	 * @param principal.
	 * @param model
	 * @return A Spring response to specify the thymeleaf template to used.
	 */
	@RequestMapping(value = "/deposit", method = RequestMethod.POST)
	public String depositPOST(@ModelAttribute("amount") String amount, Principal principal, Model model) {
		try {
			new BigDecimal(amount);
		} catch (NumberFormatException ex) {
			model.addAttribute("errordeposit", true);
			return "deposit";
		}
		accountService.deposit(amount, principal);

		return "redirect:/home";
	}

	/**
	 * Spring MVC implementation of getting the page to make a withdraw.
	 *
	 * @param model.
	 * @return A Spring response to specify the thymeleaf template to used.
	 */
	@RequestMapping(value = "/withdraw", method = RequestMethod.GET)
	public String withdraw(Model model) {
		model.addAttribute("amount", "");

		return "withdraw";
	}

	/**
	 * Spring MVC implementation of making a withdraw.
	 *
	 * @param amount.
	 * @param principal.
	 * @param model
	 * @return A Spring response to specify the thymeleaf template to used.
	 */
	@RequestMapping(value = "/withdraw", method = RequestMethod.POST)
	public String withdrawPOST(@ModelAttribute("amount") String amount, Principal principal, Model model) {
		UserEntity user = userService.findByUsername(principal.getName());
		
		if (user.getAccount().getAccountBalance().subtract(new BigDecimal(amount)).longValue() < 0) {
			model.addAttribute("errorbalance", true);
			return "withdraw";
		}
		
		try {
			new BigDecimal(amount);
		} catch (NumberFormatException ex) {
			model.addAttribute("errorwithdraw", true);
			return "withdraw";
		}
		accountService.withdraw(amount, principal);

		return "redirect:/home";
	}
}

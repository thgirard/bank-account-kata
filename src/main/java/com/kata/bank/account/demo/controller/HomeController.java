package com.kata.bank.account.demo.controller;

import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.kata.bank.account.demo.domain.AccountEntity;
import com.kata.bank.account.demo.domain.UserEntity;
import com.kata.bank.account.demo.service.UserService;

/**
 * Main Controller.
 */
@Controller
public class HomeController {

    /**
     * Logger.
     */
	private static final Logger LOG = LoggerFactory.getLogger(HomeController.class);

	 /**
     * User service
     */
	@Autowired
	private UserService userService;

	 /**
     * Default Spring MVC implementation of redirecting to the index page (login).
     *
     * @return A Spring response to specify the thymeleaf template to used.
     */
	@RequestMapping("/")
	public String home() {
		return "redirect:/index";
	}

	  /**
     * Index Spring MVC implementation of getting the index page (login).
     *
     * @return A Spring response to specify the thymeleaf template to used.
     */
	@RequestMapping("/index")
	public String index() {
		return "index";
	}

	 /**
     * Spring MVC implementation of getting the sign up page.
     *
     * @param model.
     * @return A Spring response to specify the thymeleaf template to used.
     */
	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String signup(Model model) {
		UserEntity user = new UserEntity();

		model.addAttribute("user", user);

		return "signup";
	}

	 /**
     * Spring MVC implementation of signing up.
     *
     * @param model.
     * @return A Spring response to specify the thymeleaf template to used.
     */
	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String signupPost(@ModelAttribute("user") UserEntity user, Model model) {

		if (userService.checkEmailExists(user.getEmail())) {
			
			model.addAttribute("emailExists", true);
			return "signup";
			
		} else if (userService.checkUsernameExists(user.getUsername())) {
			
			model.addAttribute("usernameExists", true);
			return "signup";
			
		} else {

			userService.createUser(user);

			return "redirect:/";
		}
	}

	 /**
     * Spring MVC implementation of getting the actual account balance.
     *
     * @param principal.
     * @param model.
     * @return A Spring response to specify the thymeleaf template to used.
     */
	@RequestMapping("/home")
	public String home(Principal principal, Model model) {
		
		UserEntity user = userService.findByUsername(principal.getName());
		AccountEntity account = user.getAccount();

		model.addAttribute("account", account);

		return "home";
	}

}

package com.kata.bank.account.demo.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kata.bank.account.demo.domain.TransactionEntity;
import com.kata.bank.account.demo.repository.TransactionRepository;
import com.kata.bank.account.demo.service.TransactionService;

/**
 * Transaction Service.
 */
@Service
public class TransactionService {

    /**
     * Logger.
     */
	private static final Logger LOG = LoggerFactory.getLogger(TransactionService.class);

    /**
     * Transaction repository
     */
	@Autowired
	private TransactionRepository transactionRepository;

    /**
     * Prepare the deposit transaction and then saved it.
     *
     * @param transaction The transaction.
     */
	public void saveDepositTransaction(TransactionEntity transaction) {
		transactionRepository.save(transaction);
	}

    /**
     * Prepare the withdraw transaction and then saved it.
     *
     * @param transaction The transaction.
     */
	public void saveWithdrawTransaction(TransactionEntity transaction) {
		transactionRepository.save(transaction);
	}

}

package com.kata.bank.account.demo.domain;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * Account entity.
 *
 * @author tgi
 */
@Entity
public class AccountEntity {

    /**
     * Internal account id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "accountId", nullable = false, updatable = false)
    private Long id;
    
    /**
     * Account number.
     */
    private int accountNumber;
    
    /**
     * Actual account balance.
     */
    private BigDecimal accountBalance;

    /**
     * List of all Transactions for this account.
     */
    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private List<TransactionEntity> transactionList;

    /**
     * Account id getter.
     *
     * @return The account id.
     */
    public Long getId() {
        return id;
    }

    /**
     * The account id setter.
     *
     * @param id The account id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Account number getter.
     *
     * @return The account number.
     */
    public int getAccountNumber() {
        return accountNumber;
    }

    /**
     * The account number setter.
     *
     * @param accountNumber The account number
     */
    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * Balance getter.
     *
     * @return The account balance.
     */
    public BigDecimal getAccountBalance() {
        return accountBalance;
    }

    /**
     * The balance setter.
     *
     * @param accountBalance The balance
     */
    public void setAccountBalance(BigDecimal accountBalance) {
        this.accountBalance = accountBalance;
    }

    /**
     * List of Transactions getter.
     *
     * @return The list of transactions.
     */
    public List<TransactionEntity> getTransactionList() {
        return transactionList;
    }

    /**
     * The list of transactions setter.
     *
     * @param transactionList The list of transactions
     */
    public void setTransactionList(List<TransactionEntity> transactionList) {
        this.transactionList = transactionList;
    }


}




package com.kata.bank.account.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main class.
 *
 */
@SpringBootApplication
public class BankAccountKataApplication {

    /**
     * Main method, used to run the application.
     *
     * @param args the command line arguments
     */
	public static void main(String[] args) {
		SpringApplication.run(BankAccountKataApplication.class, args);
	}

}

package com.kata.bank.account.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.kata.bank.account.demo.domain.UserEntity;


/**
 * User repository.
 */
public interface UserRepository extends CrudRepository<UserEntity, Long> {
    /**
     * Find a user by its username.
     *
     * @param username The username of the user.
     * @return The user if found, Optional.empty otherwise
     */
	Optional<UserEntity> findByUsername(String username);
	
    /**
     * Find a user by its email.
     *
     * @param email The email of the user.
     * @return The user if found, Optional.empty otherwise
     */
	Optional<UserEntity> findByEmail(String email);
	
    /**
     * Find all users.
     *
     * @return The list of all users
     * */
    List<UserEntity> findAll();
    
}

package com.kata.bank.account.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * User feature exception
 *
 * @author tgi
 */
@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR, reason="No such User")
public class UserException extends RuntimeException {

    /**
     * Serial UUID
     */
    private static final long serialVersionUID = 1L;

    /**
     * HTTP Status code
     */
    protected int httpStatusCode;
    
    /**
     * BAK feature exception.
     *
     * @param httpStatusCode The HTTP status code
     * @param message The exception message
     * @param cause The root cause
     */
    public UserException(int httpStatusCode, String message, Throwable cause) {
        super(message, cause);
        this.httpStatusCode = httpStatusCode;
    }

    /**
     * BAK feature exception.
     *
     * @param message The exception message
     * @param cause The root cause
     */
    public UserException(String message, Throwable cause) {
        super(message, cause);
        this.httpStatusCode = 500;
    }

    /**
     * BAK feature exception.
     */
    public UserException() {
        super();
        this.httpStatusCode = 500;
    }

    /**
     * The HTTP status code getter.
     *
     * @return The HTTP status code
     */
    public int getHttpStatusCode() {
        return this.httpStatusCode;
    }



}


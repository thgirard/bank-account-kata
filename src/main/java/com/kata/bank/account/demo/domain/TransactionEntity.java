package com.kata.bank.account.demo.domain;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


/**
 * Transaction entity.
 *
 * @author tgi
 */
@Entity
public class TransactionEntity {

    /**
     * Internal transaction id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "transactionId", nullable = false, updatable = false)
    private Long id;
    
    /**
     * Date of the transaction.
     */
    private Instant date;
    
    /**
     * Description.
     */
    private String description;
    
    /**
     * Type (deposit or withdraw).
     */
    private String type;
    
    /**
     * Amount.
     */
    private BigDecimal amount;
    
    /**
     * The balance after the transaction.
     */
    private BigDecimal availableBalance;

    /**
     * The default transaction constructor
     */
    public TransactionEntity() {
		super();
	}

    /**
     * The transaction constructor
     *
     * @param date     The date of the transaction
     * @param description       The description
     * @param type The type (deposit or withdraw)
     * @param amount The amount
     * @param availableBalance The balance after the transaction
     * @param account The account linked
     */
	public TransactionEntity(Instant date, String description, String type, String amount, BigDecimal availableBalance, AccountEntity account) {
        this.date = date;
        this.description = description;
        this.type = type;
        this.amount = new BigDecimal(amount);
        this.availableBalance = availableBalance;
        this.account = account;
    }

    /**
     * The account linked.
     */
    @ManyToOne
    @JoinColumn(name = "account_id")
    private AccountEntity account;

    /**
     * Transaction id getter.
     *
     * @return The transaction id.
     */
    public Long getId() {
        return id;
    }

    /**
     * The transaction id setter.
     *
     * @param id The transaction id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Date getter.
     *
     * @return The date of the transaction.
     */
    public Instant getDate() {
        return date;
    }

    /**
     * The date setter.
     *
     * @param date The date.
     */
    public void setDate(Instant date) {
        this.date = date;
    }

    /**
     * Description getter.
     *
     * @return The transaction id.
     */
    public String getDescription() {
        return description;
    }

    /**
     * The description setter.
     *
     * @param description The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Type getter.
     *
     * @return The type.
     */
    public String getType() {
        return type;
    }

    /**
     * The type setter.
     *
     * @param type The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Amount getter.
     *
     * @return The amount.
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * The amount setter.
     *
     * @param amount The amount
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * Available Balance getter.
     *
     * @return The available balance.
     */
    public BigDecimal getAvailableBalance() {
        return availableBalance;
    }

    /**
     * The available balance setter.
     *
     * @param availableBalance The available balance
     */
    public void setAvailableBalance(BigDecimal availableBalance) {
        this.availableBalance = availableBalance;
    }

    /**
     * Account getter.
     *
     * @return The account.
     */
    public AccountEntity getAccount() {
        return account;
    }

    /**
     * The account setter.
     *
     * @param account The account
     */
    public void setAccount(AccountEntity account) {
        this.account = account;
    }

}

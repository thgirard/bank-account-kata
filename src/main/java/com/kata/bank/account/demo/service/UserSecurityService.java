package com.kata.bank.account.demo.service;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.kata.bank.account.demo.domain.UserEntity;
import com.kata.bank.account.demo.repository.UserRepository;

/**
 * User Security Service.
 */
@Service
public class UserSecurityService implements UserDetailsService {

    /**
     * Logger.
     */
	private static final Logger LOG = LoggerFactory.getLogger(UserSecurityService.class);

    /**
     * User repository
     */
	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserEntity user = userRepository.findByUsername(username)
				.orElseThrow(() -> new UsernameNotFoundException("Username " + username + " not found"));

		GrantedAuthority authority = new SimpleGrantedAuthority("User");
		UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getUsername(),
				user.getPassword(), Arrays.asList(authority));
		return userDetails;
	}
}

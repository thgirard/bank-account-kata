package com.kata.bank.account.demo.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kata.bank.account.demo.domain.UserEntity;
import com.kata.bank.account.demo.exception.UserException;
import com.kata.bank.account.demo.repository.UserRepository;

/**
 * User Service.
 */
@Service
@Transactional
public class UserService {

    /**
     * Logger.
     */
	private static final Logger LOG = LoggerFactory.getLogger(UserService.class);

    /**
     * User repository
     */
	@Autowired
	private UserRepository userRepository;

    /**
     * Password Encoder with salt
     */
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

    /**
     * Account service
     */
	@Autowired
	private AccountService accountService;

    /**
     * Get a user given a username.
     *
     * @param username The username identifier.
     * @return The user
     */
	public UserEntity findByUsername(String username) {
		return userRepository.findByUsername(username).orElseThrow(() -> new UserException());
	}

    /**
     * Prepare the user and then saved it.
     *
     * @param user The user.
     * @return The user saved in DB
     */
	public UserEntity createUser(UserEntity user) {

		String encryptedPassword = passwordEncoder.encode(user.getPassword());
		user.setPassword(encryptedPassword);

		user.setAccount(accountService.createAccount());

		return userRepository.save(user);

	}

    /**
     * Check if user exist given a username identifier.
     *
     * @param username The username identifier.
     * @return True or false if the user exists
     */
	public boolean checkUsernameExists(String username) {
		if (userRepository.findByUsername(username).isPresent()) {
			return true;
		}

		return false;
	}

    /**
     * Check if user exist given an email identifier.
     *
     * @param email The email identifier.
     * @return True or false if the user exists
     */
	public boolean checkEmailExists(String email) {
		if (userRepository.findByEmail(email).isPresent()) {
			return true;
		}

		return false;
	}

}

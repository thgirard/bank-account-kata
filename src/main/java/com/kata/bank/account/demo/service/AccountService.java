package com.kata.bank.account.demo.service;

import java.math.BigDecimal;
import java.security.Principal;
import java.time.Instant;
import java.util.Date;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kata.bank.account.demo.domain.AccountEntity;
import com.kata.bank.account.demo.domain.TransactionEntity;
import com.kata.bank.account.demo.domain.UserEntity;
import com.kata.bank.account.demo.exception.UserException;
import com.kata.bank.account.demo.repository.AccountRepository;
import com.kata.bank.account.demo.service.AccountService;
import com.kata.bank.account.demo.service.TransactionService;
import com.kata.bank.account.demo.service.UserService;

/**
 * Account Service.
 */
@Service
public class AccountService {

    /**
     * Logger.
     */
	private static final Logger LOG = LoggerFactory.getLogger(AccountService.class);

    /**
     * Account repository
     */
	@Autowired
	private AccountRepository accountRepository;
	
    /**
     * User service
     */
	@Autowired
	private UserService userService;

    /**
     * Transaction service
     */
	@Autowired
	private TransactionService transactionService;

    /**
     * Prepare a new account and then saved it.
     *
     *
     * @return The account saved in DB
     */
	public AccountEntity createAccount() {
		AccountEntity account = new AccountEntity();
		account.setAccountBalance(new BigDecimal("0.0"));
		Random r = new Random();
		account.setAccountNumber(r.ints().findFirst().getAsInt());

		return accountRepository.save(account);

	}

    /**
     * Prepare a new deposit for a specific user and amount and then saved it.
     *
     * @param amount The amount.
     * @param principal The identity of the user.
     *
     */
	public void deposit(String amount, Principal principal) throws UserException {
		UserEntity user = userService.findByUsername(principal.getName());

		AccountEntity account = user.getAccount();
		account.setAccountBalance(account.getAccountBalance().add(new BigDecimal(amount)));
		accountRepository.save(account);

		TransactionEntity transaction = new TransactionEntity(Instant.now(), "Deposit", "Deposit", amount, account.getAccountBalance(),
				account);
		transactionService.saveDepositTransaction(transaction);

	}

    /**
     * Prepare a new withdraw for a specific user and amount and then saved it.
     *
     * @param amount The amount.
     * @param principal The identity of the user.
     *
     */
	public void withdraw(String amount, Principal principal) throws UserException {
		UserEntity user = userService.findByUsername(principal.getName());

		AccountEntity account = user.getAccount();
		account.setAccountBalance(account.getAccountBalance().subtract(new BigDecimal(amount)));
		accountRepository.save(account);

		TransactionEntity transaction = new TransactionEntity(Instant.now(), "Withdraw", "Withdraw", amount, account.getAccountBalance(),
				account);
		transactionService.saveWithdrawTransaction(transaction);

	}

}

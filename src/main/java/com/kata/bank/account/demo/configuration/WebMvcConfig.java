package com.kata.bank.account.demo.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Spring web mvc configuration file.
 *
 * @author tgi
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {

        //
        // Access some static resource contain in jar:
        //
		registry.addResourceHandler("/jquery/**") //
				.addResourceLocations("classpath:/META-INF/resources/webjars/jquery/3.0.0/");

		registry.addResourceHandler("/popper/**") //
				.addResourceLocations("classpath:/META-INF/resources/webjars/popper.js/1.14.3/umd/");

		registry.addResourceHandler("/bootstrap/**") //
				.addResourceLocations("classpath:/META-INF/resources/webjars/bootstrap/4.3.1/");

	}

}

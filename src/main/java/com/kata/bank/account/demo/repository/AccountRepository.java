package com.kata.bank.account.demo.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.kata.bank.account.demo.domain.AccountEntity;

/**
 * Bank Account repository.
 */
public interface AccountRepository extends CrudRepository<AccountEntity, Long>{
	
    /**
     * Find an account by its accountNumber.
     *
     * @param accountNumber The accountNumber of the account.
     * @return The account if found, Optional.empty otherwise
     */
	Optional<AccountEntity> findByAccountNumber (int accountNumber);

}

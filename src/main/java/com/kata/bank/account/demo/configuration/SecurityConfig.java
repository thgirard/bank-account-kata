package com.kata.bank.account.demo.configuration;

import java.security.SecureRandom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.kata.bank.account.demo.service.UserSecurityService;

/**
 * Spring security configuration file.
 *
 * @author tgi
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * User security Service
     */
	@Autowired
	private UserSecurityService userSecurityService;

    /**
     * Salt string to used
     */
	private static final String SALT = "fsdfs";
	
    /**
     * List of paths with no authentication
     */
	private static final String[] PUBLIC_MATCHERS = { "/bootstrap/**", "/popper/**", "/jquery/**", "/css/**", "/",
			"/error", "/signup" };

    /**
     * Customize the default passordEncoder of Spring Security.
     */
	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		//Create password encoder with salt to avoid attack offline 
		//Better solution could be to used BCrypt.gensalt() and add it in the UserEntity
		return new BCryptPasswordEncoder(9, new SecureRandom(SALT.getBytes()));
	}


	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		//add authentication for every requests except some paths 
		//disable csrf and add path for login/logout
		http.authorizeRequests().antMatchers(PUBLIC_MATCHERS).permitAll().anyRequest().authenticated();


		http.csrf().disable().cors().disable().formLogin().failureUrl("/index?error").defaultSuccessUrl("/home")
				.loginPage("/index").permitAll().and().logout()
				.logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/index?logout");
	}

    /**
     * Customize the method to used to authenticate a specific user.
     * 
     * @param AuthenticationManagerBuilder The AuthenticationManagerBuilder.
     * 
     */
	@Autowired
	public void addCustomAuthentification(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
		authenticationManagerBuilder.userDetailsService(userSecurityService).passwordEncoder(passwordEncoder());
	}
}

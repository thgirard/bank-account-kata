package com.kata.bank.account.demo.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;


/**
 * User entity.
 *
 * @author tgi
 */
@Entity
public class UserEntity {

    /**
     * Internal user id.
     */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "userId", nullable = false, updatable = false)
	private Long userId;
	
    /**
     * Username.
     */
	private String username;
	
    /**
     * Password encrypted.
     */
	private String password;
	
    /**
     * Firstname.
     */
	private String firstName;
	
    /**
     * Lastname
     */
	private String lastName;

    /**
     * Unique email.
     */
	@Column(name = "email", nullable = false, unique = true)
	private String email;

    /**
     * Account linked.
     */
	@OneToOne
	private AccountEntity account;
	//At the momement just one account by user

	
    /**
     * User id getter.
     *
     * @return The user id.
     */
	public Long getUserId() {
		return userId;
	}

    /**
     * The user id setter.
     *
     * @param userId The user id
     */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

    /**
     * Username getter.
     *
     * @return The username.
     */
	public String getUsername() {
		return username;
	}

    /**
     * The username setter.
     *
     * @param username The username
     */
	public void setUsername(String username) {
		this.username = username;
	}

    /**
     * Firstname getter.
     *
     * @return The firstname.
     */
	public String getFirstName() {
		return firstName;
	}

    /**
     * The firstname setter.
     *
     * @param firstname The firstname
     */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

    /**
     * Lastname getter.
     *
     * @return The lastname.
     */
	public String getLastName() {
		return lastName;
	}

    /**
     * The lastname.
     *
     * @param lastname The lastname
     */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

    /**
     * Email getter.
     *
     * @return The email.
     */
	public String getEmail() {
		return email;
	}

    /**
     * The email setter.
     *
     * @param email The email
     */
	public void setEmail(String email) {
		this.email = email;
	}

    /**
     * Password getter.
     *
     * @return The password.
     */
	public String getPassword() {
		return password;
	}

    /**
     * The encrypted password setter.
     *
     * @param password The encrypted password
     */
	public void setPassword(String password) {
		this.password = password;
	}

    /**
     * Account getter.
     *
     * @return The account.
     */
	public AccountEntity getAccount() {
		return account;
	}

    /**
     * The account setter.
     *
     * @param account The account
     */
	public void setAccount(AccountEntity account) {
		this.account = account;
	}

}

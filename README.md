# Getting Started


BANK ACCOUNT KATA(BAK)
======================================================

This is a Spring Boot MVC Thymeleaf application. 
It was made using Spring Boot, Thymeleaf, Spring Data JPA. Database is in memory H2.
The purpose of this application is to implements this requirements :
- Deposit and Withdrawal
- Account statement (date, amount, balance)
- Statement printing

# Development
---

## Requirements

To work with this project, the following tools are needed :

1. [JDK 11] (https://jdk.java.net/java-se-ri/11)
2. [Maven 3](https://maven.apache.org/download.cgi)

## Project structure and tips

### Structure

The project structure has been initialized as following

```shell
├── BankAccountKata               # Implementation classes for the application
      ├── src
      │   ├── main
      │   │   ├── java
      │   │   └── resources
      │   │       ├── templates                      # Thymeleaf templates
      │   │       ├── application.properties         # Spring configuration file
      │   ├── test

```

### In-Memory development database

When running the application on development workstation, an in-memory [H2] database is used to avoid a dependency to a shared database or a heavy local one. 

```shell
  spring.datasource.url
  spring.datasource.username
  spring.datasource.password
```

### Configuration

The files [application.yml](src/main/resources/application.properties) must be updated to match your development environment.

The properties to update are :

```properties
	server.port
```

## Compilation and Unit tests

```shell
  mvnw test
```

## Run on development workstation

1. Development profile

```shell
  mvnw clean install && mvnw spring-boot:run
```

## Packaging

```shell
  mvnw package
```

The packaging of BAK will create a an executable JAR file in :

```shell
  BankAccountKata\target\BankAccountKata-<version>-SNAPSHOT.jar
```

That file can be executed as follow :

```
  java -jar BankAccountKata\target\BankAccountKata-<version>-SNAPSHOT.jar
```
The application should be up and running within a few seconds.

Go to the web browser and visit http://localhost:8083

H2 Database web interface
Go to the web browser and visit http://localhost:8083/h2




### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.1.9.RELEASE/maven-plugin/)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/2.1.9.RELEASE/reference/htmlsingle/#boot-features-jpa-and-spring-data)
* [Spring Security](https://docs.spring.io/spring-boot/docs/2.1.9.RELEASE/reference/htmlsingle/#boot-features-security)

### Guides
The following guides illustrate how to use some features concretely:

* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)
* [Securing a Web Application](https://spring.io/guides/gs/securing-web/)

